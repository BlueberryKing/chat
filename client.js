$(() => {
	const log = [];
	const input = $("#messages");

	const MESSAGES = {
		MOVED_FORWARD: "hat den Roboter forwärts bewegt.",
		MOVED_BACKWARD: "hat den Roboter rückwärts bewegt.",
		MOVED_LEFT: "hat den Roboter nach links bewegt.",
		MOVED_RIGHT: "hat den Roboter nach rechts bewegt.",
		OPENED_CLAW: "hat den Greifer geöffnet.",
		CLOSED_CLAW: "hat den Greifer geschlossen."
	}

	window.WebSocket = window.WebSocket || window.MozWebSocket;

	if (!window.WebSocket) {
		console.error("Your browser doesn't seem to support WebSockets");
		return;
	}

	const connection = new WebSocket("ws://127.0.0.1:8000");
	connection.onerror = (error) => handleError(error);
	connection.onmessage = (res) => handleMessage(res);

	$(document).keydown((e) => {
		// const message = MESSAGES[e.keyCode & MESSAGES.length];
		send(MESSAGES.MOVED_FORWARD);
	})

	function send(message) {
		waitForConnection(() => {
			connection.send(message);
		}, 1000);
	}

	function waitForConnection(callback, interval) {
		if (connection.readyState === 1) {
			callback();
		} else {
			interval = interval || 1000;

			setTimeout(() => {
				waitForConnection(callback, interval);
			}, interval);
		}
	}

	function addMessage(author, color, message, time) {
		log.push(`[${time}] ${author}: ${message}`);

		const name = `<span style=\"font-weight:bold; color:${color}\">${author}:</span>`
		input.append(`<div class="message">[${time}] ${name} ${message}</div>`);
	}

	function handleError(error) {
		console.error("Connection could not be established.");
	}

	function handleMessage(res) {
		let message;

		try {
			message = JSON.parse(res.data);
		} catch (e) {
			console.log("Invalid JSON: ", res.data);
			return;
		}

		const data = message.data;
		console.log("data:", data);

		if (message.type === "history") { 
			for (let i = 0; i < data.length; i++) {
				addMessage(data[i].author, data[i].color, data[i].text, data[i].time);
			}
		} else if (message.type === "message") {
			addMessage(data.author, data.color, data.text, data.time);
		} else {
			console.error("Error parsing JSON:", json);
		}
	}
});